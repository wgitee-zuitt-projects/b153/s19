//getCube

let getCube = 2 ** 3;

console.log(`The cube of 2 is ${getCube}`)

//Address
const address = ["258 Washington Ave NW", "California 90011"]

let [address1, address2] = address

console.log(`I live at ${address1}, ${address2}`)

//Animal

const animal = {
	animalName: "Lolong",
	animalFamily: "crocodile",
	animalWeight: "1075 kgs",
	animalLength: "20 ft 3 in"
}

let = {animalName, animalFamily, animalWeight, animalLength} = animal

console.log(`${animalName} was a saltwater ${animalFamily}. He weighed at ${animalWeight} with a measurement of ${animalLength}.`)

//array of Numbers

const numbers = [4, 5, 9, 14, 23, 37, 60]


numbers.forEach((number)=> {
			console.log(number)
		})

// reduceNumber - sum of all numbers in the array
const reduceNumber = numbers.reduce((sum, a) => sum + a, 0)

console.log(reduceNumber)