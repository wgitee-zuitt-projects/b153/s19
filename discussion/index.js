// ES6 stands for ECMAScript 2015 and is an update to the perviius versions of ECMAScript. ECMAScript is the standard that is used to create the implementations of the language, one of which is JavaScript.

//EXPONENT OPERATOR- ** (power) or Math.pow() method

let firstNum = 8 ** 2

console.log(firstNum) //64

//Math.pow() - used for complex mathematical operations in ES6
let secondNum = Math.pow(8,2)// 64

console.log(secondNum)

//Documentation for JS Math Object: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math

//TEMPLATE LITERALS

let greeting = "Hello!"
console.log(greeting + " My name is" + " Will")

let song = "Careless Whisper";
let artist = "George Michael";

console.log(`My favorite song is ${song} by ${artist}`) //concatenation using ES6

//OBJECT DESTRUCTURING

/*const person = {
	firstName: "John",
	middleName: "Jacob",
	lastName: "Smith"
}

let = {firstName, middleName, lastName } = person

console.log(firstName)
console.log(middleName)
console.log(lastName)*/

// ARRAY DESTRUCTURING

const people = ["John", "Joe", "Jack"]

let [firstPerson, secondPerson, thirdPerson] = people

console.log(`The first person is ${firstPerson}`)
console.log(`The second person is ${secondPerson}`)
console.log(`The third person is ${thirdPerson}`)

// USING THE .keys() METHOD TO LOOP THROUGH OBJECTS

const person = {
	firstName: "John",
	middleName: "Jacob",
	lastName: "Smith"
}

const personKeys = Object.keys(person)

//Object.keys() creates an array populated with the keys/ properties of the object passed to its parameter

console.log(personKeys)

personKeys.forEach(function(key){
	console.log(typeof person[key])
})

// Same:
// person.firstName
// person[firstName]

//Object.values() can also be used to create an array populated with the values of the object passed as an argument


/*
		ARROW FUNCTIONS

		- Compact alternatives syntax to writing traditional functions

		const functionName = () => {
			code to execute
		}

*/

/*function sayHello(){
	console.log("Hello")
}*/

// Can also be rewritten as

const sayHello = () => {
	console.log("Hello")
}

/*function addNum(num){
	return num + num
}*/

// the above code can also be rewritten as:

const addNum = num => num + num

// Exclusive to Arrow Functions, certain symbols can be motted depending on the situation

//The parenthses around the argument can be removed if exactly ONE argument is needed to the function

// The curly braces can be removed if your function only needs to execute a single statement

// Example -> const addNums = (x, y) => x + y

const sayThings = () => {
	console.log("hello")
	console.log("Nice to meet you!")
}

// Arrow functions have what are called "implicit return statements"

//const add = (x, y) => x + y

const add = (x, y) => {
	return x + y
}

let total = add(1, 2)

console.log(total)